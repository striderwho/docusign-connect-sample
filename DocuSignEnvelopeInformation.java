package com.jenscompany.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by jennifer.cheung on 7/10/2016.
 */
@XmlRootElement(name = "DocuSignEnvelopeInformation")
public class DocuSignEnvelopeInformation {
    private EnvelopeStatus envelopeStatus;

    public DocuSignEnvelopeInformation() {}

    public DocuSignEnvelopeInformation(EnvelopeStatus envelope) {
        this.envelopeStatus = envelope;
    }

    public EnvelopeStatus getEnvelopeStatus() {
        return this.envelopeStatus;
    }

    @XmlElement(name="EnvelopeStatus")
    public void setEnvelopeStatus(EnvelopeStatus envelopeStatus) {
        this.envelopeStatus = envelopeStatus;
    }

    public String getEnvelopeId() {
        return this.envelopeStatus.getEnvelopeID();
    }
}
