<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import="com.jenscompany.database.DatabaseConnectionManager"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>JSP List Users Records</title>
</head>
<body>
    <sql:setDataSource
        var="myDS"
        driver="org.hsqldb.jdbc.JDBCDriver"
        url="jdbc:hsqldb:/var/docusignconnect/connectdb"
        user="user" password="c0nn4c6"
    />

    <sql:query var="getAllEnvelopeStatuses"   dataSource="${myDS}">
        SELECT * FROM envelopestatus;
    </sql:query>

    <div align="center">
        <table border="1" cellpadding="5">
            <caption><h2>List of envelopes</h2></caption>
            <tr>
                <th>Envelope ID</th>
                <th>Subject</th>
                <th>Username</th>
                <th>Email</th>
                <th>Status</th>
                <th>Send</th>
            </tr>
            <c:forEach var="envelopestatus" items="${getAllEnvelopeStatuses.rows}">
                <tr>
                    <td><c:out value="${envelopestatus.EnvelopeID}" /></td>
                    <td><c:out value="${envelopestatus.Subject}" /></td>
                    <td><c:out value="${envelopestatus.UserName}" /></td>
                    <td><c:out value="${envelopestatus.Email}" /></td>
                    <td><c:out value="${envelopestatus.Status}" /></td>
                    <td><c:out value="${envelopestatus.Sent}" /></td>
                </tr>
            </c:forEach>
        </table>
    </div>
</body>
</html>