package com.jenscompany.database;

import com.jenscompany.model.DocuSignEnvelopeInformation;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by jennifer.cheung on 12/10/2016.
 */
public interface EnvelopeInfoDao {
    public List<DocuSignEnvelopeInformation> getAllEnvelopeStatuses() throws SQLException;

    public DocuSignEnvelopeInformation getEnvelopeInfo(String envelopeId) throws SQLException;

    public void updateEnvelopeInfo(DocuSignEnvelopeInformation envelope) throws SQLException;

    public void deleteEnvelopeInfo(DocuSignEnvelopeInformation envelope) throws SQLException;
}
