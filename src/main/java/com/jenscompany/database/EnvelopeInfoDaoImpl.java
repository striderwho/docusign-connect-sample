package com.jenscompany.database;

import com.jenscompany.model.DocuSignEnvelopeInformation;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by jennifer.cheung on 12/10/2016.
 */
public class EnvelopeInfoDaoImpl implements EnvelopeInfoDao {
    DatabaseConnectionManager databaseConn;

    public EnvelopeInfoDaoImpl() {
        try {
            databaseConn = DatabaseConnectionManager.getInstance();
        } catch (Exception e) {
            System.out.println("Error creating database connection");
        }
    }

    @Override
    public List<DocuSignEnvelopeInformation> getAllEnvelopeStatuses() throws SQLException {
        return null;
    }

    @Override
    public DocuSignEnvelopeInformation getEnvelopeInfo(String envelopeId) throws SQLException {
        return null;
    }

    @Override
    public void updateEnvelopeInfo(DocuSignEnvelopeInformation envelope) throws SQLException {
        DocuSignEnvelopeInformation.EnvelopeStatus status = envelope.getEnvelopeStatus();
        String values = "('" + status.getEnvelopeID() + "','"
                            + status.getSubject() + "','"
                            + status.getUserName() + "','"
                            + status.getEmail() + "','"
                            + status.getStatus() + "','"
                            + status.getSent() + "')";
        String query = "INSERT INTO envelopestatus(EnvelopeID, Subject, UserName, Email, Status, Sent) VALUES" + values;

        databaseConn.update(query);
        databaseConn.shutdown();
    }

    @Override
    public void deleteEnvelopeInfo(DocuSignEnvelopeInformation envelope) throws SQLException {

    }
}
