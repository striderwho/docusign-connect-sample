package com.jenscompany.database;

import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.Properties;

/**
 * Created by jennifer.cheung on 12/10/2016.
 */
public class DatabaseConnectionManager {
    /**
     * @TODO move to configuration file
     */
    public static String DATABASE_PATH = ""; //"C:\\Users\\jennifer.cheung\\Box Sync\\DocuSign Info\\Dev\\DocuSign Connect Sample\\";
    public static String DATABASE_NAME = ""; //"connectdb";
    public static String DATABASE_USER = ""; //"user";
    public static String DATABASE_PASSWD = ""; //"c0nn4c6";

    private static DatabaseConnectionManager instance = null;
    static Connection conn;

    public DatabaseConnectionManager() throws Exception {
        Class.forName("org.hsqldb.jdbcDriver");

        readDbProperties();

        conn = DriverManager.getConnection("jdbc:hsqldb:" + DATABASE_PATH + DATABASE_NAME, DATABASE_USER, DATABASE_PASSWD);
    }

    // Want this to be a singleton class
    public static DatabaseConnectionManager getInstance() throws Exception {
        if(instance == null) {
            return new DatabaseConnectionManager();
        }
        else
            return instance;
    }

    public void shutdown() throws SQLException {

        Statement st = conn.createStatement();

        // db writes out to files and performs clean shuts down
        // otherwise there will be an unclean shutdown
        // when program ends
        st.execute("SHUTDOWN");
        conn.close();    // if there are no other open connection
    }

    //use for SQL command SELECT
    public synchronized void query(String expression) throws SQLException {

        Statement st = null;
        ResultSet rs = null;

        st = conn.createStatement();
        rs = st.executeQuery(expression);    // run the query

        // do something with the result set.
        dump(rs);
        st.close();
    }

    //use for SQL commands CREATE, DROP, INSERT and UPDATE
    public synchronized void update(String expression) throws SQLException {

        Statement st = null;

        st = conn.createStatement();    // statements

        int i = st.executeUpdate(expression);    // run the query

        if (i == -1) {
            System.out.println("db error : " + expression);
        }

        st.close();
    }

    public void dump(ResultSet rs) throws SQLException {

        // the order of the rows in a cursor
        // are implementation dependent unless you use the SQL ORDER statement
        ResultSetMetaData meta = rs.getMetaData();
        int colmax = meta.getColumnCount();
        int i;
        Object o = null;

        // the result set is a cursor into the data.  You can only
        // point to one row at a time
        // assume we are pointing to BEFORE the first row
        // rs.next() points to next row and returns true
        // or false if there is no next row, which breaks the loop
        for (; rs.next(); ) {
            for (i = 0; i < colmax; ++i) {
                o = rs.getObject(i + 1);    // Is SQL the first column is indexed

                // with 1 not 0
                System.out.print(o.toString() + " ");
            }

            System.out.println(" ");
        }

    }

    private void readDbProperties() {
        Properties prop = new Properties();
        InputStream input = null;

        try {

            String filename = "dbconfig.properties";
            input = DatabaseConnectionManager.class.getClassLoader().getResourceAsStream(filename);
            if(input==null){
                System.out.println("Sorry, unable to find " + filename);
                return;
            }

            //load a properties file from class path, inside static method
            prop.load(input);

            //get the property value and print it out
            System.out.println(prop.getProperty("dbpath"));

            DATABASE_PATH = prop.getProperty("dbpath");
            DATABASE_NAME = prop.getProperty("dbname");
            DATABASE_USER = prop.getProperty("dbuser");
            DATABASE_PASSWD = prop.getProperty("dbpass");

            // Check if any details are missing
            if(DATABASE_PATH.isEmpty() || DATABASE_NAME.isEmpty() || DATABASE_USER.isEmpty() || DATABASE_PASSWD.isEmpty()) {
                throw new Exception("Database details are missing");
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception ex2) {
            ex2.printStackTrace();
        } finally {
            if(input!=null){
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
