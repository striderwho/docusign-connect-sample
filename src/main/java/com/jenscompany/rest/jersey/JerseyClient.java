package com.jenscompany.rest.jersey;

import com.jenscompany.model.DocuSignEnvelopeInformation;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class JerseyClient {

    public static void main(String[] args) {
        try {

            //EnvelopeStatus st = new EnvelopeStatus("sent", "1234", "Subject!");
            DocuSignEnvelopeInformation env = new DocuSignEnvelopeInformation();

            Client client = Client.create();

            WebResource webResource = client.resource("http://localhost:8080/docusign-connect-0.1-SNAPSHOT/rest/connectlistener/");

            ClientResponse response = webResource.accept("application/xml")
                    .post(ClientResponse.class, env);

            if (response.getStatus() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatus());
            }

            String output = response.getEntity(String.class);

            System.out.println("Server response : \n");
            System.out.println(output);

        } catch (Exception e) {

            e.printStackTrace();

        }

    }

}