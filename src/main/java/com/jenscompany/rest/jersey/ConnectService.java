package com.jenscompany.rest.jersey;

import com.jenscompany.database.EnvelopeInfoDao;
import com.jenscompany.database.EnvelopeInfoDaoImpl;
import com.jenscompany.model.DocuSignEnvelopeInformation;

import javax.print.attribute.standard.Media;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.SQLException;

/**
 * Created by jennifer.cheung on 7/10/2016.
 */

@Path("/connectlistener")
public class ConnectService {

    @POST
    //@Path("/send")
    @Consumes(MediaType.TEXT_XML)
    //@Produces(MediaType.TEXT_PLAIN)
    public Response consumeXML(DocuSignEnvelopeInformation envelope) {

        String output = "Envelope ID: "+envelope.getEnvelopeStatus().getEnvelopeID();
        EnvelopeInfoDao envelopeDao = new EnvelopeInfoDaoImpl();

        try {
            envelopeDao.updateEnvelopeInfo(envelope);
        } catch (SQLException s) {
            output += "\nError updating database: " + s.getMessage();
            return Response.status(500).entity(output).build();
        }

        return Response.status(200).entity(output).build();
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response ping() {
        return Response.status(200).entity("I'm alive!").build();
    }

}
