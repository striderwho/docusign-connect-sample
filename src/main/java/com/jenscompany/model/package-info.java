/**
 * Created by jennifer.cheung on 14/10/2016.
 */

@javax.xml.bind.annotation.XmlSchema(namespace="http://www.docusign.net/API/3.0", elementFormDefault = QUALIFIED)
package com.jenscompany.model;

import static javax.xml.bind.annotation.XmlNsForm.QUALIFIED;