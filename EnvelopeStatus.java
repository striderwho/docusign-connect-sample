package com.jenscompany.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by jennifer.cheung on 10/10/2016.
 */
//@XmlRootElement
public class EnvelopeStatus {
    String envelopeID;
    String subject;
    String status;

    public EnvelopeStatus() {
    }

    public EnvelopeStatus(String status, String envelopeID, String subject) {
        this.status = status;
        this.envelopeID = envelopeID;
        this.subject = subject;
    }

    public String getEnvelopeID() {
        return envelopeID;
    }

    @XmlElement(name="EnvelopeID")
    public void setEnvelopeID(String envelopeID) {
        this.envelopeID = envelopeID;
    }

    public String getSubject() {
        return subject;
    }

    @XmlElement(name="Subject")
    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getStatus() {
        return status;
    }

    @XmlElement(name="Status")
    public void setStatus(String status) {
        this.status = status;
    }
}
